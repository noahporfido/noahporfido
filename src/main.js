import "animate.css";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import KsVueFullpage from "ks-vue-fullpage";
import "hammerjs";
import "velocity-animate";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserSecret, faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

Vue.component("font-awesome-icon", FontAwesomeIcon);
library.add(faUserSecret, faChevronDown);

[{ plugin: KsVueFullpage }].forEach((x) =>
  Vue.use(x.plugin, x.params ? x.params : null)
);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
