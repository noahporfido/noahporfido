import Vue from "vue";
import VueRouter from "vue-router";
import ViewContainer from "../views/ViewContainer/ViewContainer.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "ViewContainer",
    component: ViewContainer
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
