import { Start, About, Project } from "@/components/Sections";

export default {
  name: "viewContainer",
  components: { Start, About, Project },
  data() {
    return {
      sections: [
        { id: 1, componentName: "start", backgroundColor: "white" },
        { id: 2, componentName: "about", backgroundColor: "white" },
        { id: 3, componentName: "project", backgroundColor: "white" },
      ],
      options: {
        overlay: false,
        preloaderEnabled: false,
        dotNavEnabled: false,
        duration: 500,
      },
    };
  },
};
