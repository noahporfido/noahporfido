import ProjectCard from "./ProjectCard/ProjectCard.vue";

export default {
  name: "ProjectSection",
  components: { ProjectCard },
  data() {
    return {
      offsetX: 0,
      mousePos: 0,
      cards: [
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
        { title: "test", desc: "desc", img: "https://picsum.photos/400/600" },
      ],
    };
  },
  methods: {
    calcOffsetX(i) {
      this.offsetX = (100 / this.cards.length) * (i + 1) * -1;
      const screenWidth = window.innerWidth;

      const itemsWidth =
        document.querySelector(".card-container").offsetWidth + 40; // 40 is for the margin

      console.log(itemsWidth);
      // this.offsetX = c * -1;
    },
  },
  watch: {
    mousePos(newVal, oldVal) {
      console.log("mousepos", newVal);
      const screenWidth = window.innerWidth;
    },
  },
};
