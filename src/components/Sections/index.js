export { default as About } from "./About/About.vue";

export { default as Start } from "./Start/Start.vue";

export { default as Project } from "./Project/Project.vue";
