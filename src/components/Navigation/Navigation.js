export default {
  name: "Navigation",
  data() {
    return { expanded: false };
  },
  methods: {
    redirect(index) {
      this.expandMenuToggle();
      this.$ksvuefp.$emit("ksvuefp-nav-click", { nextIndex: index });
    },
    expandMenuToggle() {
      this.expanded = !this.expanded;
    },
  },
  computed: {
    isStartScreen() {
      return this.$ksvuefp.currentIndex === 0;
    },
    currentScreen() {
      return this.$ksvuefp.currentIndex;
    },
  },
};
